defmodule Botconicita do
  @moduledoc """
  Documentation for `Botconicita`.
  """

  def token do
    System.get_env("CONICITA_BOT_TOKEN")
  end

  # this is the chat id where the bot dumps usage statistics
  # every day 
  def dump_statistics_chat_id do
    System.get_env("DUMP_STATISTICS_CHAT_ID")
  end

  def send_usage_statistics do
    IO.inspect("send usate statistics")
    token = Botconicita.token()
    chat_id = Botconicita.dump_statistics_chat_id()
    {total, daily} = GenServer.call(Botconicita.Statistics.GenServer, :get)

    if daily > 0 do
      Telegram.Api.request(
        token,
        "sendMessage",
        chat_id: chat_id,
        text: """
        Il numero totali di richieste ricevuto
        dall'ultimo riavvio è #{total}.
        Il numero totale di richieste ricevuto
        oggi è #{daily}.
        """,
        parse_mode: "Markdown",
        disable_notification: True
      )
    end

    GenServer.call(Botconicita.Statistics.GenServer, :reset_daily)
  end

  def calculate_conicity(value) do
    IO.inspect("calculate conicity")
    res = Integer.parse(value)

    message =
      case res do
        {parsed_value, _} when parsed_value != 0 ->
          new_value = 2 * ElixirMath.atan(1 / (parsed_value * 2)) * 57.2957795

          """
          Il valore di conicità `1:#{parsed_value}`
          corrisponde ad un angolo
          di `#{Float.round(new_value, 4)} gradi.`
          """
        {0, _} ->
          """
          Il valore richiesto non è valido.
          Ho ricevuto il numero `0`.
          Il numero `0` non è un valore valido
          di conicità.

          Se vuoi conoscere il valore dell'angolo per una conicità di valore 20,
          inviami il messaggio `20`.
          Risponderò con il valore dell'angolo.
          """
        :error ->
          """
          Il valore richiesto non è un numero.
          Ho ricevuto:

          ```
          #{value}
          ```
          Mi aspettavo un numero.
          Se vuoi conoscere il valore dell'angolo per una conicità di valore 20,
          inviami il messaggio `20`.
          Risponderò con il valore dell'angolo.
          """
      end

    message
  end
end
