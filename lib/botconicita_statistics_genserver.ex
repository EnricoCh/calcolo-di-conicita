defmodule Botconicita.Statistics.GenServer do
  use GenServer

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, :initial_state, name: Botconicita.Statistics.GenServer)
  end

  @impl true
  def init(:initial_state) do
    {:ok, %{total: 0, daily: 0}}
  end

  @impl true
  def handle_call(:next, _from, state) do
    total = state.total + 1
    daily = state.daily + 1
    {:reply, 0, %{total: total, daily: daily}}
  end

  @impl true
  def handle_call(:reset_daily, _from, state) do
    {:reply, 0, %{state | daily: 0}}
  end

  @impl true
  def handle_call(:get, _from, state) do
    total = state.total
    daily = state.daily
    {:reply, {total, daily}, state}
  end
end
