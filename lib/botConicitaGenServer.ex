# # questo modulo è stato rimpiazzato da BotconicitaBOT
# defmodule Botconicita.GenServer do
#   use GenServer, restart: :permanent

#   @time 500

#   def start_link(default) do
#     GenServer.start_link(__MODULE__, 0, default)
#   end

#   @impl true
#   def init(_) do
#     # se questa linea fallisce il token è probabilmente errato (o non settato)
#     {:ok, _} =
#       Telegram.Api.request(
#         Botconicita.token(),
#         "getMe"
#       )

#     Process.send_after(self(), :wait_message, 100_000)
#     {:ok, 0}
#   end

#   @impl
#   def handle_info(:wait_message, state) do
#     token = Botconicita.token()

#     {:ok, [update | _]} =
#       Telegram.Api.request(
#         token,
#         "getUpdates",
#         offset: -1,
#         timeout: 30
#       )

#     state =
#       if state != update do
#         state = update
#         chat_id = update["message"]["chat"]["id"]
#         message_id = update["message"]["message_id"]
#         incoming_message = update["message"]["text"]

#         output_message = Botconicita.calculate_conicity(incoming_message)

#         Telegram.Api.request(
#           token,
#           "sendMessage",
#           chat_id: chat_id,
#           text: output_message,
#           parse_mode: "Markdown",
#           reply_to_message_id: message_id
#         )

#         # slows the application after a message
#         Process.sleep(@time)
#         state
#       else
#         state
#       end

#     Process.send_after(self(), :wait_message, @time)
#     {:noreply, state}
#   end
# end
