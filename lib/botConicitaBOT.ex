defmodule BotconicitaBOT do
  @behaviour Telegram.Bot

  @impl true
  def handle_update(update, token) do
    chat_id = update["message"]["chat"]["id"]
    message_id = update["message"]["message_id"]
    incoming_message = update["message"]["text"]

    output_message = Botconicita.calculate_conicity(incoming_message)

    GenServer.call(Botconicita.Statistics.GenServer, :next)

    Telegram.Api.request(
      token,
      "sendMessage",
      chat_id: chat_id,
      text: output_message,
      parse_mode: "Markdown",
      reply_to_message_id: message_id
    )

    {:noreply, token}
  end
end
