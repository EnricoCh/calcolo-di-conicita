defmodule Botconicita.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    token = Botconicita.token()
    options = [purge: true, max_bot_concurrency: 1_000]

    children = [
      Botconicita.Scheduler,
      Botconicita.Statistics.GenServer,
      {Telegram.Bot.Async.Supervisor, {BotconicitaBOT, token, options}}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    # ,name: ConicitaBotApplication]
    opts = [strategy: :one_for_one]
    Supervisor.start_link(children, opts)
  end
end
