import Config

config :botconicita, Botconicita.Scheduler,
  jobs: [
    {"30 22 * * *", fn -> Botconicita.send_usage_statistics end},
  ]
